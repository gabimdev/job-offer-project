const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const jobOfferSchema = new Schema({
    contactEmail: { type: String, required: true },
    title: { type: String, required: true },
    description: { type: String, required: true },
    company: { type: String, required: true },
    salary: { type: Number, required: false },
    image: { type: String },
    creatorId: { type: mongoose.Types.ObjectId, ref: 'User' },
}, {
    timestamps: true,
});

const JobOffer = mongoose.model('JobOffer', jobOfferSchema);

module.exports = JobOffer;