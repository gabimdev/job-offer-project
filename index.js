require('dotenv').config();
const express = require('express');
const passport = require('passport');
const session = require('express-session');
const sassMiddleware = require('node-sass-middleware');
const MongoStore = require('connect-mongo');
const path = require('path');
const methodOverride = require('method-override');
const db = require('./db.js');
const hbs = require('hbs');
const indexRoutes = require('./routes/index.routes');
const jobOffersIndex = require('./routes/jobOffers.routes');
const authRoutes = require('./routes/auth.routes');
const manageOffersRoutes = require('./routes/manageOffers.routes');
const manageUserRoutes = require('./routes/manageUsers.routes');
const authMiddleware = require('./middlewares/auth.middleware');


require('./passport/passport');

db.connect();

const { PORT } = process.env || 3000;
const { SESSION_SECRET } = process.env

const app = express();


app.use(session({
    secret: SESSION_SECRET,
    resave: false,
    saveUninitialized: false,
    cookie: {
        maxAge: 48 * 60 * 60 * 1000
    },
    store: MongoStore.create({ mongoUrl: db.DB_URL }),
}));

app.use(sassMiddleware({
    src: path.join(__dirname, 'public'),
    dest: path.join(__dirname, 'public'),
    outputStyle: 'compressed',
}));

app.use(passport.initialize());
app.use(passport.session());

app.use(methodOverride('_method'));

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use(express.static(path.join(__dirname, 'public/assets')));
app.use(express.static(path.join(__dirname, 'public')));
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');
hbs.registerPartials(__dirname + '/views/partials');

app.use('/', indexRoutes);
app.use('/job', jobOffersIndex);
app.use('/auth', authRoutes);
app.use('/manage', [authMiddleware.isAuthenticated], manageOffersRoutes);
app.use('/admin', manageUserRoutes);

app.use('*', (req, res, next) => {
    const error = new Error('Route not Found');
    error.status = 404;
    next(error)
});

app.use((error, req, res, next) => {
    error.message = error.message || 'Unexpected error';
    return res.status(error.status || 500).render('error', { error, user: req.user })
});


app.listen(PORT, () => {
    console.log(`Server listening in http://localhost:${PORT}`);
});