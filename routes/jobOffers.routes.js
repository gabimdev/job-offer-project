const express = require('express');
const JobOffers = require('../models/JobOffer');
const { upload, uploadToCloudnary } = require('../middlewares/file.mddleware');

const router = express.Router();

router.get('/offers', async(req, res, next) => {
    try {
        const jobOffers = await JobOffers.find();
        return res.render('jobOffers', { jobOffers, user: req.user, });
    } catch (error) {
        console.log(error);
        next(error);
    }
});

router.get('/detail/:id', async(req, res, next) => {
    try {
        const { id } = req.params;
        const offerDetail = await JobOffers.findById(id);
        return res.render('offerDetail', { offerDetail, user: req.user });
    } catch (error) {
        console.log(error);
        next(error);
    }
});

router.get('/salary/', async(req, res, next) => {
    try {
        const {  salary } = req.query
        const jobOffersBySalary = await JobOffers.find({ salary: { $gte: salary } });
        return res.render('jobOffers', { jobOffersBySalary });
    } catch (error) {
        console.log(error);
        next(error);
    }
});


router.post('/create', [upload.single('image'), uploadToCloudnary], async(req, res, next) => {
    const offerImage = req.file_url;
    try {
        const {
            contactEmail,
            title,
            description,
            company,
            salary,
            image = offerImage,
            creatorId = req.user._id,
        } = req.body;

        const newJobOffer = new JobOffers({
            contactEmail,
            title,
            description,
            company,
            salary,
            image,
            creatorId,
        });
        const savedJobOffer = await newJobOffer.save();
        return res.redirect('/manage/offers');
    } catch (error) {
        return res.render('index', { error: error.message });
    }
});

router.delete('/delete/:id', async(req, res, next) => {
    try {
        const { id } = req.params;
        const deleted = await JobOffers.findByIdAndDelete(id);
        if (deleted) {
            return res.status(200).redirect("/manage/offers")
        }
        return res.status(200).redirect("/manage/offers");
    } catch (error) {
        next(error);
    }
})

module.exports = router;