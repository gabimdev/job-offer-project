const express = require('express');
const User = require('../models/User');
const autMiddleware = require('../middlewares/auth.middleware');

const router = express.Router();

router.get('/users', [autMiddleware.isAdmin], async(req, res, next) => {
    try {
        const listUsers = await User.find();
        return res.render('adminUsers', { listUsers });
    } catch (error) {
        console.log(error);
        next(error);
    }
});

module.exports = router;