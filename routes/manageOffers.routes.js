const express = require('express');
const JobOffers = require('../models/JobOffer');
const { upload, uploadToCloudnary } = require('../middlewares/file.mddleware');


const router = express.Router();

router.get('/offers', async(req, res, next) => {
    const userRole = req.user.role;
    const creatorId = req.user._id;
    if (userRole === 'admin') {
        try {
            const jobOffers = await JobOffers.find();
            res.status(200).render('manage', { title: 'Job Offers', user: req.user, jobOffers: jobOffers, admin: req.user.role === 'admin' })
        } catch (error) {
            next(error);
        }
    } else {
        try {
            const jobOffers = await JobOffers.find({ creatorId });
            console.log(jobOffers);
            res.status(200).render('manage', { title: 'Job Offers', user: req.user, jobOffers: jobOffers, admin: req.user.role === 'admin' })
        } catch (error) {
            next(error);
        }
    }


});

router.get('/modify/:id', async(req, res, next) => {
    try {
        const { id } = req.params;
        const offerDetail = await JobOffers.findById(id);
        return res.render('modifyOffer', { offerDetail, user: req.user });
    } catch (error) {
        console.log(error);
        next(error);
    }
});

router.put('/edit', [upload.single('image'), uploadToCloudnary], async(req, res, next) => {
    const offerImage = req.file_url;
    try {
        const { id } = req.body;
        const updateOffer = await JobOffers.findByIdAndUpdate(id, {
            contactEmail: req.body.contactEmail,
            title: req.body.title,
            description: req.body.description,
            company: req.body.company,
            salary: req.body.salary,
            image: offerImage,
        }, {
            new: true
        });
        return res.redirect('/manage/offers');
    } catch (error) {
        next(error);
    }
})

module.exports = router;