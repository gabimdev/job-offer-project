const express = require('express');
const passport = require('passport');

const router = express.Router();

router.get('/register', (req, res, next) => {
    return res.render('register');
})

router.post('/register', (req, res, next) => {
    const { email, password } = req.body;
    if (!email || !password) {
        const error = new Error('User and password are required');
        return res.render('register', { error: error.message });
    }
    passport.authenticate('register', (error, user) => {
        if (error) {
            return res.render('register', { error: error.message });
        }
        return res.redirect('/auth/login');
    })(req);
});

router.get('/login', (req, res, next) => {
    return res.render('login');
})

router.post('/login', (req, res, next) => {
    const { email, password } = req.body;
    if (!email || !password)  {
        const error = new Error('User and password are required')
        return res.render('login', { error: error.message });
    }
    passport.authenticate('login', (error, user) => {
        if (error) {
            return res.render('login', { error: error.message });
        }
        req.logIn(user, (error) => {
            if (error) {
                return res.render('login');
            }
            return res.redirect('/');
        })
    })(req, res, next)
});


router.post('/logout', (req, res, next) => {
    if (req.user) {
        req.logout();
        req.session.destroy(() => {
            res.clearCookie('connect.sid');
            return res.render('index');
        })
    } else {
        return res.redirect('/');
    }
});


module.exports = router;