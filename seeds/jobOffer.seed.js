require('dotenv').config();
const mongoose = require('mongoose');
const db = require('../db');
const JobOffer = require('../models/JobOffer');

const jobOffers = [{
        contactEmail: "upgrade@email.com",
        title: "Programador",
        description: "Programador Full Stack",
        salary: 21000,
        company: "Upgrade Hub",
    },
    {
        contactEmail: "google@email.com",
        title: "Developer",
        description: "Programador angular",
        salary: 25000,
        company: "Google",
    },
];

mongoose.connect(db.DB_URL, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
}).then(async() => {
    const allJobOffers = await JobOffer.find();
    if (allJobOffers.length) {
        console.log('Deleting all JobOffers...');
        await JobOffer.collection.drop();
    }
}).catch(error => {
    console.log('Error deleting data', error)
}).then(async() => {
    await JobOffer.insertMany(jobOffers);
    console.log('Sucessfully add jobOffers to DB...')
}).catch(error => {
    console.log('Error creating data:', error);
}).finally(() => mongoose.disconnect());