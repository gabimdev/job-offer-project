const mongoose = require('mongoose');

//DB_URL=mongodb://localhost:27017/jobOffers
const { DB_URL } = process.env;

const connect = async() => {
    try {
        await mongoose.connect(DB_URL, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
        });
        console.log('Conectado a la DB')
    } catch (error) {
        console.log('Error conectano a la base de datos', error)
    }
}

module.exports = { connect, DB_URL }