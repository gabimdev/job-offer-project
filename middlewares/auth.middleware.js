const isAuthenticated = (req, res, next) => {
    if (req.isAuthenticated()) {
        return next();
    } else {
        return res.redirect('/login');
    }
}

const isAdmin = (req, res, next) => {
    if (req.isAuthenticated()) {
        if (req.user.role === 'admin') {
            next();
        } else {
            const error = new Error('No tienes permiso de administrador');
            error.status = 403;
            return next(error);
        }
    } else {
        return res.redirect('/login');
    }
}

module.exports = { isAuthenticated, isAdmin };